# Backend Mobile CRUD App (Shopping List) - SIP Test

Kode backend untuk aplikasi mobile (Shopping List) menggunakan laravel 10

## Tech Stack

**Database :** MySQL

**Backend :** Laravel 10

### Clone dan masuk ke project

```bash
  git clone https://gitlab.com/sip-tests/mobile-crud-backend.git
  cd mobile-crud-backend
```

### Setting env

Buat file .env

-   Buat file .env dan salin isi .env_example ke .env

atau jalankan perintah

```bash
  cp .env_example .env
```

### Setup Database

-   Buat database dengan nama db_mobile_crud

#### Setting env untuk database

```bash
DB_DATABASE=db_mobile_crud
DB_USERNAME=root
DB_PASSWORD=
```

### Instalasi dependensi dan buat key aplikasi

```bash
composer install
```

```bash
php artisan key:generate
```

### Migrasi

```bash
php artisan migrate
```

### Running aplikasi

```bash
php artisan serve
```
