<?php

namespace App\Http\Controllers;

use App\Models\Shopping;
use Illuminate\Http\Request;

class ShoppingController extends Controller
{
    /**
     * Display a listing of the resource.
     */
    public function index()
    {
        $shoppings = Shopping::all();
        return response()->json($shoppings);
    }

    public function store(Request $request)
    {
        $data = $request->validate([
            'name' => 'required',
            'description' => 'nullable',
        ]);

        $shopping = Shopping::create($data);
        return response()->json($shopping, 201);
    }

    public function update(Request $request, $id)
    {
        $shopping = Shopping::find($id);

        if (!$shopping) {
            return response()->json(['message' => 'Shopping not found'], 404);
        }

        $shopping->update($request->all());
        return response()->json($shopping, 200);
    }

    public function destroy($id)
    {
        $shopping = Shopping::find($id);

        if (!$shopping) {
            return response()->json(['message' => 'Shopping not found'], 404);
        }

        $shopping->delete();
        return response()->json(['message' => 'Shopping deleted'], 200);
    }

}
